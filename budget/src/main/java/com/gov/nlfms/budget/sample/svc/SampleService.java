package com.gov.nlfms.budget.sample.svc;

import com.gov.nlfms.common.message.MessageConvertor;
import org.springframework.stereotype.Service;

@Service
public class SampleService {
    public String sampleString(String pageName) {
        MessageConvertor messageConvertor = new MessageConvertor();
        String message = messageConvertor.convert(pageName, "*");

        return message;
    }
}
