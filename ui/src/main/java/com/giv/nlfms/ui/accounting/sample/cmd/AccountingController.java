package com.giv.nlfms.ui.accounting.sample.cmd;

import com.gov.nlfms.accounting.sample.svc.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountingController {

    @GetMapping("/accounting/sample")
    public String initView(){
        SampleService sampleService = new SampleService();

        String message = sampleService.sampleString("accounting");

        return message;
    }
}
